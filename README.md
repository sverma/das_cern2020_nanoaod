git clone https://:@gitlab.cern.ch:8443/gouskos/das_cern2020_nanoaod.git

cd das_cern2020_nanoaod

cmsrel CMSSW_11_2_0_pre3 

cd CMSSW_11_2_0_pre3/src/

cmsenv

git clone https://github.com/cms-nanoAOD/nanoAOD-tools.git NanoAODTools

scram b -j8

cd PhysicsTools/NanoAODTools

